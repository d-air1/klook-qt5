/* KLook
 * Copyright (c) 2011-2012 ROSA  <support@rosalab.ru>
 * Authors: Julia Mineeva, Evgeniy Auzhin, Sergey Borovkov.
 * License: GPLv3
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 3,
 *   or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "audio.h"

#include <KEncodingProber>
#include <QtCore/QFileInfo>
#include <QtCore/QString>
#include <QtCore/QTextCodec>

#include <QAudioOutput>
#include <QMediaObject>
#include <QVideoWidget>

Audio::Audio(QQuickItem* parent)
    : QQuickItem(parent)
    , m_isPreview(false)
    , m_isReady(false)
{
    m_mediaObject = new QMediaPlayer;
    m_mediaObject->setNotifyInterval(1000);

    QObject::connect(m_mediaObject, SIGNAL(tick(qint64)), SLOT(onTicked(qint64)));
    QObject::connect(m_mediaObject, SIGNAL(totalTimeChanged(qint64)), SLOT(onTotalTimeChanged(qint64)));
    QObject::connect(m_mediaObject, SIGNAL(finished()), SLOT(onFinished()));
    QObject::connect(m_mediaObject, SIGNAL(stateChanged(QMediaPlayer::State, QMediaPlayer::State)),
                     SLOT(stateChanged(QMediaPlayer::State, QMediaPlayer::State)));
}

Audio::~Audio()
{
    delete m_mediaObject;
}

void Audio::setPause()
{
    if (!isVisible()) {
        pause();
    }
    else {
        play();
    }
}

bool Audio::isReady()
{
    return m_isReady;
}

void Audio::setReady(bool b)
{
    m_isReady = b;
    emit ready();
}

void Audio::onFinished()
{
    emit playFinished();
    setPosition(0);
}

QString Audio::source() const
{
    return m_mediaObject->media().request().url().toString();
}

void Audio::setSource(const QUrl &source)
{
    m_mediaObject->setMedia(QUrl::fromLocalFile(source.toString()));
    emit sourceChanged();
}

qint64 Audio::position() const
{
    return m_mediaObject->position();
}

void Audio::setPosition(qint64 pos)
{
    m_mediaObject->setPosition(pos);
    emit positionChanged();
}

qlonglong Audio::totalTime() const
{
    return m_mediaObject->duration();
}

QTime Audio::duration()
{
    QTime t;
    t = t.addMSecs(m_mediaObject->duration());
    return t;
}

void Audio::onTotalTimeChanged(qint64 t)
{
    Q_UNUSED(t)
    emit totalTimeChanged();
}

void Audio::onTicked(qint64 t)
{
    emit ticked(QVariant(t));
}

bool Audio::playing() const
{
    return (m_mediaObject->state() == QMediaPlayer::State::PlayingState);
}

bool Audio::paused() const
{
    return (m_mediaObject->state() == QMediaPlayer::State::PausedState);
}

void Audio::play()
{
    m_mediaObject->play();
}

void Audio::pause()
{
    m_mediaObject->pause();
}

void Audio::play_or_pause()
{
    if (playing()) {
        pause();
    }
    else {
        play();
    }
}

bool Audio::isPreview() const
{
    return m_isPreview;
}

void Audio::setPreview(bool preview)
{
    m_isPreview = preview;
}

void Audio::stateChanged(QMediaPlayer::State newState, QMediaPlayer::State oldState)
{
    Q_UNUSED(newState)
    if ((oldState == QMediaPlayer::LoadingMedia) || (oldState == QMediaPlayer::StoppedState)) {
        setReady(true);
    }
}

QString Audio::artist()
{
    QString artists;
    QStringList listArtists = m_mediaObject->metaData("ARTIST").toStringList();
    if (!listArtists.empty()) {
        artists = guessEncoding(listArtists.join(", ").toUtf8());
    }
    return artists;
}

QString Audio::album()
{
    QString albums;
    QStringList listAlbums = m_mediaObject->metaData("ALBUM").toStringList();
    if (!listAlbums.empty()) {
        albums = guessEncoding(listAlbums.join(", ").toUtf8());
    }
    return albums;
}

QString Audio::title()
{
    QString titles;
    QStringList listTitles = m_mediaObject->metaData("TITLE").toStringList();

    titles = guessEncoding(listTitles.join(", ").toUtf8());
    if (titles.isEmpty()) {
        QFileInfo fi(m_mediaObject->media().request().url().path());
        titles = fi.fileName();
    }

    return titles;
}

QString Audio::guessEncoding(const QByteArray &data)
{
    KEncodingProber prober(KEncodingProber::Universal);
    prober.feed(data);
    QString str = prober.confidence() > 0.7
            ? QTextCodec::codecForName(prober.encoding())->toUnicode(data)
            : QString::fromUtf8(data);

    return str;
}

QString Audio::genre()
{
    QString genres;
    QStringList listgenres = m_mediaObject->metaData("GENRE").toStringList();
    if (!listgenres.empty()) {
        genres = guessEncoding(listgenres.join(", ").toUtf8());
    }

    return genres;
}

